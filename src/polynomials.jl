module polynomials

export polynomial, monomial

import Base.length, Base.endof, Base.copy, Base.zero, Base.one
import Base.show, Base.*, Base./, Base.-, Base.+, Base.==
import Base.sort, Base.sort!, Base.isequal
import Base.convert, Base.nnz
import Base.setindex!
import Base.getindex

type monomial{T<:Number}
    # Univariate monomial.
    order::Int
    coeff::T
    function monomial(order::Int, coeff::T)
        new(order, coeff)
    end
end

# Set default value for the coefficient equal to 1
monomial{T<:Number}(order::Int, coeff::T = one(Int)) = monomial{T}(order, coeff)

# copy method for polynomials
function copy{T<:Number}(m::monomial{T})
    return monomial(m.order,m.coeff)
end

# Type conversion
function convert{T<:Number, S<:Number}(::Type{T}, m::monomial{S})
    coeff = convert(T,m.coeff)
    return monomial(m.order, coeff)
end

# Multiplication of monomials
function *{T<:Number, S<:Number}(m::monomial{T}, n::monomial{S})
    return monomial(m.order+n.order, m.coeff*n.coeff)
end

# Division of monomials
function /{T<:Number, S<:Number}(m::monomial{T}, n::monomial{S})
    if m.order>=n.order
        return monomial(m.order-n.order, m.coeff/n.coeff)
    else
        throw(DivideError())
    end
end

# Addition/Soustraction of monomials
for op = (:+, :-)
    @eval begin
        function ($op){T<:Number, S<:Number}(m::monomial{T}, n::monomial{S})
            if m.order==n.order
                return monomial(m.order, ($op)(m.coeff,n.coeff))
            else
                # The order of the second monomial must be inferior or equal to the number of the first monomial.
                throw(ArgumentError())
            end
        end
    end
end

# Unary minus operator for monomials
function -{T<:Number}(m::monomial{T})
    n = copy(m)
    n.coeff = -n.coeff
    return n
end

type polynomial{T<:Number}
    #
    # Univariate polynomial of order at most n, where m=n+1 is the number of elements in input vector a:
    #
    # P(x) = a[1]*x^n + a[2]*x^(n-1) + ... + a[n]*x + a[n+1]
    #
    # The default value of order, length(a)-1, is reduced if vector a contains zeros in the first positions.
    #
    a::Vector{T}                                # The vector of coefficient values.
    order::Int                                  # The order of the polynomial.
    nzp::Vector{Int}                            # The vector of integers corresponding to powers associated with non zero coefficients.
    function polynomial(a::Vector{T})
        j = 1
        m = length(a)
        n = m-1
        nzp = zeros(Int, m)                     # If all the coefficient are non zero, then nzp has m elements, otherwise nzp is smaller... 
        for i=1:m
            if a[i]!=0
                nzp[j] = m-i
                j = j+1
            end
        end
        a = a[a.!=0]                            # Remove the zero coefficients.
        nzp = nzp[1:j-1]                        # Cut nzp and keep only the non zero values.
        if isempty(nzp)
            # zero polynomial
            a = [0]
            order = 0
        else
            # non zero polynomial
            order = max(nzp)                    # Get the actual order of the polynomial.
        end
        new(a, order, nzp)
    end
    function polynomial(a::Vector{T}, nzp::Vector{Int})
        order = max(nzp)
        new(a, order, nzp)
    end
    function polynomial(value::T, order::Int)
        nnn = order+1
        nzp = zeros(Int,nnn)
        for i=1:nnn
            nzp[i] = nnn-i
        end
        a = zeros(T,order+1)
        if abs(value)>0
            fill!(a,value)
        end
        new(a, order, nzp)
    end
end

polynomial{T<:Number}(a::Vector{T}) = polynomial{T}(a)
polynomial{T<:Number}(a::Vector{T}, nzp::Vector{Int}) = polynomial{T}(a, nzp)
polynomial{T<:Number}(value::T, order::Int) = polynomial{T}(value, order)
polynomial{T<:Number}(m::monomial{T}) = polynomial{T}([m.coeff],[m.order])

# Display polynomial
function show(io::IO, p::polynomial)
    print(io,"Polynomial of order $(p.order):\n")
    for k in 1:length(p.nzp)
        l = p.nzp[k]
        c = p[l]
        if k>1
            if c>0
                print(io," + ")
            else
                if l>0
                    print(io," - ")
                end
            end
        end
        if isa(c,Real) && isequal(c, one(typeof(c))) # Test if the coefficient is equal to one.
            if isequal(l,0)
                print(io, "1")
            end
        elseif isa(c,Real) && isequal(abs(c), one(typeof(c)))  # Test if the coefficient is equal to minus one.
            if  k>1
                if isequal(l,0)
                    print(io, " - 1")
                end
            else
                if isequal(l,0)
                    print(io, "-1")
                end
            end
        else
            print(io, "$(abs(c))*")
        end
        if ~isequal(l,0)
            if ~isequal(l,1)
                print(io, "X^$(l)")
            else
                print(io, "X")
            end
        end
    end
end

# Type conversion
function convert{T<:Number, S<:Number}(::Type{T}, p::polynomial{S})
    return polynomial(convert(Array{T,1},copy(p.a)),copy(p.nzp))
end

# length of a polynomial is its order.
length(p::polynomial) = p.order

# number of non zero monomials in polynomial p.
function nnz{T<:Number}(p::polynomial{T})
    return length(p.nzp)
end

function isfull{T<:Number}(p::polynomial{T})
    return (nnz(p)==(p.order+1) && p.nzp==p.order:-1:0)
end

# Reference. p[i] is the coefficient associated with the power i of x.
function getindex{T<:Number}(p::polynomial{T}, i::Int)
    if i>p.order || i<0
        error("Index has to be non negative and less than polynomial order!")
    end
    j = find(p.nzp.==i)
    if isempty(j)
        return zero(T)
    else
        return p.a[j[1]]
    end
end

# Reference. p[v], where v is a vector of powers returns a vector with length(v) elements. Element i is the coefficient associated with the power v[i] of x.
function getindex{T<:Number}(p::polynomial{T}, v::Vector{Int})
    q = zeros(T,length(v))
    j = 1
    for power in v
        q[j] = p[power]
        j = j+1
    end
    return q
end

# Reference. p[v], where v is a range of integers returns a vector with length(v) elements. Element i is the coefficient associated with the power v[i] of x.
function getindex{T<:Number}(p::polynomial{T}, v::Range1{Int})
    q = zeros(T,length(v))
    j = 1
    for power in v
        q[j] = p[power]
        j = j+1
    end
    return q
end

getindex{T<:Number}(p::polynomial{T}, v::Colon) = getindex(p, 0:endof(p))

# pack function. Removes monomials with zero coefficients.
function pack{T<:Number}(p::polynomial{T})
    q = copy(p)
    pack!(q)
end

function sparse{T<:Number}(p::polynomial{T})
    return pack(p)
end

# (in place) pack function. Removes monomials with zero coefficients.
function pack!{T<:Number}(p::polynomial{T})
    test_for_non_zero_coefficients = (p.a .!= 0)
    if !all(test_for_non_zero_coefficients)
        p.a = p.a[test_for_non_zero_coefficients]
        p.nzp = p.nzp[test_for_non_zero_coefficients]
        p.order = max(p.nzp)
    end
end

# (in place) sort function. Sort the function by decreasing order of the powers.
function sort!{T<:Number}(p::polynomial{T})
    tmp = hcat(p.nzp, p.a)
    tmp = sortrows(tmp,rev=true)
    p.nzp = tmp[:,1]
    p.a = tmp[:,2]
end

# full method (adds zero coefficients)
function full!{T<:Number}(p::polynomial{T})
    if !isfull(p)
        a = zeros(T,p.order+1)
        for i=p.order:-1:0
            a[end-i] = p[i]
        end
        p.a  = a
        p.nzp = p.order:-1:0
    end
end

function full{T<:Number}(p::polynomial{T})
    q = copy(p)
    full!(q)
    return q
end

function isequal{T<:Number}(p::polynomial{T}, q::polynomial{T})
    if !isequal(p.order,q.order)
        return false
    end
    if !isequal(p.nzp,q.nzp)
        return false
    end
    if !isequal(p.a,q.a)
        return false
    end
    return true
end

function isequal{T<:Number}(p::polynomial{T}, c::T)
    if !isequal(p.order,0)
        return false
    end
    if !isequal(p.a[1],c)
        return false
    end
    return true
end

function monomial{T<:Number}(p::polynomial{T},power::Int)
    return monomial(power,p[power])
end

function lead{T<:Number}(p::polynomial{T})
    return monomial(p, p.order)
end

# p[end] is the coefficient associated to the monomial with the largest power
endof(p::polynomial) = p.order

# Assign. Syntax p[i] = v set the coefficient associated to the power i of x equal to v  
function setindex!(p::polynomial, v::Number, i::Int)
    if i>p.order || i<0
        error("Index has to be non negative and less than the polynomial order!")
    end
    j = find(p.nzp.==i)
    if !isempty(j)
        p.a[j[1]] = v
    else
        error(string("Polynomial p has no monomial of order ", i, "!"))
    end
end

# Assign. Syntax p[I] = v, where I is an n*1 vector or range of powers and v is a scalar
for t=(:(Vector{Int}), :(Range1{Int}))
    @eval begin
        function setindex!(p::polynomial, v::Number, I::($t))
            for i=length(I):-1:1
                if I[i]>p.order || I[i]<0
                    error("Index has to be non negative and less than the polynomial order!")
                end
                j = find(p.nzp.==I[i])
                if !isempty(j)
                    p.a[j[1]] = v
                else
                    error(string("Polynomial p has no monomial of order ", I[i], "!"))
                end
            end
        end
    end
end

# Assign. Syntax p[I] = V, where I is an n*1 vector or range of powers and V is a vector
for t=(:(Vector{Int}), :(Range1{Int}))
    for u=(:(Int), :(Uint), :(BigInt), :(Float64), :(Float32), :(BigFloat), :(Rational{Int}), :(Rational{Uint}), :(Complex))
        # I don't understand why the previous loop is necessary... But Julia doesn't find the method if
        # the type of the second input argument is not specified more explicitely (ie V::Vector{Number}
        # does not work).
        @eval begin
            function setindex!(p::polynomial, V::Vector{($u)}, I::($t))
                for i=length(I):-1:1
                    if I[i]>p.order || I[i]<0
                        error("Index has to be non negative and less than the polynomial order!")
                    end
                    j = find(p.nzp.==I[i])
                    if !isempty(j)
                        p.a[j[1]] = V[i]
                    else
                        error(string("Polynomial p has no monomial of order ", I[i], "!"))
                    end
                end
            end
        end
    end
end

# copy method for polynomials
function copy{T<:Number}(p::polynomial{T})
    r = polynomial(zeros(T,p.order+1))
    r.a = copy(p.a)
    r.order = p.order
    r.nzp = copy(p.nzp)
    return r
end

# Multiplication/Division of a polynomial and a constant
for op = (:*, :/, ://)
    @eval begin
        function ($op){T<:Number,S<:Number}(p::polynomial{T},c::S)
            U = promote_type(T,S)
            a = convert(Array{U,1},copy(p.a))
            d = convert(U,copy(c))
            r = ($op)(a,d)
            return polynomial(r,copy(p.nzp))
        end
    end
end

*{T<:Number, S<:Number}(c::S, p::polynomial{T}) = *(p,c)


# Multiply polynomial p by a monomial m
*{T<:Number, S<:Number}(m::monomial{S}, p::polynomial{T}) = *(p, m)
*{T<:Number, S<:Number}(p::polynomial{T}, m::monomial{S}) = *(p, m)

function *{T<:Number, S<:Number}(p::polynomial{T}, m::monomial{S})
    U = promote_type(T,S)
    q = convert(U,p)
    n = convert(U,m)
    q.nzp = q.nzp + n.order
    q.order = q.order + n.order
    q.a = q.a*n.coeff
    return q
end

# Multiply polynomials p and q
*{T<:Number,S<:Number}(p::polynomial{T}, q::polynomial{S}) = *(p, q)

function *{T<:Number,S<:Number}(p::polynomial{T},q::polynomial{S})
    # Initialize an order p.order.q.order polynomial with zero coefficients.
    U = promote_type(T,S)
    r = polynomial(zero(U),p.order+q.order)
    # Compute the product of p and q
    for i in p.nzp
        for j in q.nzp
            r[i+j] += p[i]*q[j]
        end
    end
    return r
end

# Add/Substract two polynomials p and q
for op = (:+, :-)
    @eval begin
        function ($op){T<:Number,S<:Number}(p::polynomial{T},q::polynomial{S})
            U = promote_type(T,S)
            pp = convert(U,p)
            qq = convert(U,q)
            pack!(pp)
            pack!(qq)
            if nnz(pp)<nnz(qq)
                rr = polynomial(qq.a,qq.nzp)
                for i=1:nnz(pp)
                    rr = ($op)(rr,monomial(pp,pp.nzp[i]))
                end
            else
                rr = polynomial(pp.a,pp.nzp)
                for i=1:nnz(qq)
                    rr = ($op)(rr,monomial(qq,qq.nzp[i]))
                end
            end
            return rr
        end
    end
end

# Add/Substract a polynomial p and a monomial m
for op = (:+, :-)
    @eval begin
        function ($op){T<:Number, S<:Number}(p::polynomial{T}, m::monomial{S})
            if m.order==0
                # Add a constant
                return $(op)(p,m.coeff)
            elseif m.order>p.order
                U = promote_type(T,S)
                q = convert(U,p)
                q.a = vcat(zero(U),q.a)
                q.nzp = vcat(m.order,q.nzp)
                q.order = m.order
                q[m.order] = ($op)(q[m.order],convert(U,m.coeff))
                return q
            else
                j = find(p.nzp.==m.order)
                if isempty(j)
                    U = promote_type(T,S)
                    q = convert(U,p)
                    q.a = vcat(zero(U),q.a)
                    q.a[1] = ($op)(q.a[1],convert(U,m.coeff))
                    q.nzp = vcat(m.order,q.nzp)
                    sort!(q)
                    return q
                else
                    U = promote_type(T,S)
                    q = convert(U,p)
                    q[m.order] = ($op)(q[m.order],convert(U,m.coeff))
                    return q
                end
            end
        end
    end
end

+{T<:Number, S<:Number}(m::monomial{S}, p::polynomial{T}) = +(p, m)
-{T<:Number, S<:Number}(m::monomial{S}, p::polynomial{T}) = +(-p, m)

# Add/Substract a polynomial p and a constant
for op = (:+, :-)
    @eval begin
        function ($op){T<:Number, S<:Number}(p::polynomial{T}, c::S)
            U = promote_type(T,S)
            q = convert(U,p)
            if q.nzp[end]!=0
                q.a = vcat(q.a,zero(U))
                q.nzp = vcat(q.nzp,0)
            end
            q[0] = ($op)(q[0],convert(U,c))
            return(q)
        end
    end
end

+{T<:Number, S<:Number}(c::S, p::polynomial{T}) = +(p, c)
-{T<:Number, S<:Number}(c::S, p::polynomial{T}) = +(-p, c)

# Unary minus for polynomials
function -{T<:Number}(p::polynomial{T})
    q = copy(p)
    q.a = -q.a
    return q
end

# Evaluation of a polynomial p at x (scalar)
function evaluate{T<:Number, S<:Number}(p::polynomial{T}, x::S)
    # Use Horner algorithm to evaluate the polynomial p at x
    value = p[end]
    for i=p.order-1:-1:0
        value = x*value+p[i]
    end
    return value
end

# Euclidian division of polynomials p and q
function /{T<:Number, S<:Number}(p::polynomial{T},d::polynomial{S})
    U = promote_type(T,S)
    q = zero(U)
    r = convert(U,p)
    while !isequal(r,0) && r.order>=d.order
        v = lead(r)/lead(d)
        q = q+polynomial(v)
        r = r - v*d
        pack!(r)
    end
    return q, r
end

# Computes derivates of a polynomial
function derivates{T<:Number}(p::polynomial{T}, n::Int)
    a = copy(p.a)
    nzp = copy(p.nzp)
    a = a.*nzp
    nzp = nzp-1
    idx = find(nzp.>=0)
    a = a[idx]
    nzp = nzp[idx]
    if isempty(nzp)
        q = zero(T)
    else
        q = polynomial(a,nzp)
    end
    if n==1 || isequal(q,zero(T))
        return q
    else
        derivates(q, n-1)
    end
end


end
